﻿using Microsoft.AspNetCore.Mvc;
using Prueba.Common;
using Prueba.Services;

namespace Prueba.API.Controllers
{
    [Route("api/cliente")]
    [ApiController]
    public class ClienteController : ControllerBase
    {
        private readonly IClienteService _servicio;
        public ClienteController( IClienteService servicio)
        {
            _servicio = servicio;
        }

        // Metodo que lista los clientes.
        [HttpGet]
        public ActionResult<List<ClienteDto>> Lista()
        {
            var retorno = _servicio.ObtenerClientes();
            return retorno;            
        }

        // Metodo que manda a guardar un nuevo cliente
        [HttpPost]
        public ActionResult<string> InsertarCliente(ClienteDto clienteDto)
        {
            try
            {
                var retorno = _servicio.InsertarCliente(clienteDto);
                return retorno;
            } catch (Exception ex)
            {
                return ex.ToString();
            }
        }
    }
}
