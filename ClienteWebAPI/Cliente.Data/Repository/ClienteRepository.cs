﻿using Microsoft.EntityFrameworkCore;
using Oracle.ManagedDataAccess.Client;
using Prueba.Data.Models;
using System.Data;
using Oracle.ManagedDataAccess.Types;

namespace Prueba.Data.Repository
{
    public class ClienteRepository: IClienteRepository
    {
        // Se establece la conexion con la base de datos, esta conexion se utiliza unicamente en el metodo Listar()
        // Creo que este tipo de conexion no es una buena practica..
        OracleConnection CONEXION = new OracleConnection("User Id=C##ADMIN;Password=123456;Data Source=localhost:1521/xe;");

        // Metodo que manda a insertar un cliente nuevo.
        // retorna el ID del cliente que acaba de ser insertado.
        public string InsertarCliente(Cliente cliente)
        {
            var IdCliente = new OracleParameter("p_Id_Cliente", OracleDbType.Int32);
            IdCliente.Direction = ParameterDirection.Output;
            using (ModelContext ctx = new ModelContext())
            {
                ctx.Database.ExecuteSqlInterpolated($@"BEGIN GESTION_K_CLIENTE.insertarcliente({cliente.Nombre}, {cliente.Apellidos}, {cliente.Telefono}, {cliente.Activo}, {IdCliente} ); END;");
            }
            return IdCliente.Value.ToString();
        }

        // Metodo que manda a obtener a todos los clientes en base de datos.
        public List<Cliente> ObtenerClientes()
        {
            CONEXION.Open();
            List<Cliente> lista = new List<Cliente>(); 
            OracleCommand cmd = new OracleCommand("GESTION_K_CLIENTE.obtenercliente", CONEXION);
            cmd.CommandType = CommandType.StoredProcedure;
            OracleParameter output = cmd.Parameters.Add("list_refcursor", OracleDbType.RefCursor);
            output.Direction = ParameterDirection.ReturnValue;
            cmd.ExecuteNonQuery();
            OracleDataReader reader = ((OracleRefCursor)output.Value).GetDataReader();
            while (reader.Read())
            {
                Cliente cliente = new Cliente();
                cliente.IdCliente = reader.GetInt16(0);
                cliente.Nombre = reader.GetString(1);
                cliente.Apellidos = reader.GetString(2);
                // Se valida que el valor del telefono no venga vacio.
                if (reader.GetValue(3).ToString() != "")
                {
                    cliente.Telefono = reader.GetString(3);
                }                    
                cliente.Activo = reader.GetString(4);
                lista.Add(cliente);
            }
            CONEXION.Close();
            return lista;
        }
    }
}
