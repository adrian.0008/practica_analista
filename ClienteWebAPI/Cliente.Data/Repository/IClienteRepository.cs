﻿using Prueba.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba.Data.Repository
{
    public interface IClienteRepository
    {
        List<Cliente> ObtenerClientes();
        string InsertarCliente(Cliente cliente);
    }
}
