﻿using System;
using System.Collections.Generic;

namespace Prueba.Data.Models
{
    public partial class Cliente
    {
        public short IdCliente { get; set; }
        public string Nombre { get; set; } = null!;
        public string Apellidos { get; set; } = null!;
        public string? Telefono { get; set; }
        public string Activo { get; set; } = null!;
    }
}
