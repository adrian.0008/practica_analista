﻿using Prueba.Common;

namespace Prueba.Services
{
    public interface IClienteService
    {
        List<ClienteDto> ObtenerClientes();
        string InsertarCliente(ClienteDto clienteDto);
    }
}
