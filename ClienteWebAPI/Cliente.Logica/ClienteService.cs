﻿using Prueba.Common;
using Prueba.Data.Models;
using Prueba.Data.Repository;


namespace Prueba.Services
{
    public class ClienteService : IClienteService
    {
        private readonly IClienteRepository _repository;
        public ClienteService(IClienteRepository repository)
        {
            _repository = repository;
        }

        public string InsertarCliente(ClienteDto clienteDto)
        {
            // Se valida que los campos "Nombre, apellidos y activo no vengan vacios."
            if (clienteDto.Nombre == "" || clienteDto.Apellidos == "" || clienteDto.Activo == "")
            {
                return "";
            }
            Cliente entidad = new Cliente();
            entidad.Nombre = clienteDto.Nombre;
            entidad.Apellidos = clienteDto.Apellidos;
            entidad.Telefono = clienteDto.Telefono;
            entidad.Activo = clienteDto.Activo;
            var resultado = _repository.InsertarCliente(entidad);
            return resultado;
        }

        public List<ClienteDto> ObtenerClientes()
        {
            List<ClienteDto> result = new List<ClienteDto>();
            var lista = _repository.ObtenerClientes();
            foreach (var entidad in lista)
            {
                ClienteDto Dto = new ClienteDto();
                Dto.IdCliente = entidad.IdCliente;
                Dto.Nombre = entidad.Nombre;
                Dto.Apellidos = entidad.Apellidos;
                Dto.Telefono = entidad.Telefono;
                Dto.Activo = entidad.Activo;
                result.Add(Dto);
            }
            return result;
        }
    }
}
