
import Crear_Cliente from "./cliente/Crear_Cliente";
import Lista_Cliente from "./cliente/Lista_Cliente";
import LandingPage from "./LandingPage";
import RedirectToLanding from "./utils/RedirectToLanding";

const routes = [
    // Cliente
    { path: "/cliente/crear", component: Crear_Cliente },
    { path: "/cliente", component: Lista_Cliente, exact: true},
    { path: "/", component: LandingPage },
    { path: "*", component: RedirectToLanding },
];

export default routes;