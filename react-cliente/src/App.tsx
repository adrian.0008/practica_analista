import React from 'react';
import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Menu from './utils/Menu';
import routes from './route-config';

function App() {
  return (
    <BrowserRouter>
        <Menu />
        <div className="container-xl" id="menu">
          <Switch>
            {routes.map(routes => 
            <Route key={routes.path} path={routes.path}
              exact={routes.exact}>
                <routes.component />
              </Route>)}
          </Switch>
        </div>
      </BrowserRouter>
  );
}

export default App;
