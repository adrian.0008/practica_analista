import axios from "axios";
import { useState } from "react";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import { urlCliente } from "../utils/endpoints";
import ShowErrors from "../utils/ShowErrors";
import Form_Cliente from "./Form_Cliente";
import { clienteCrearDTO } from "./Modelo_Cliente";

export default function Crear_Cliente() {
  const history = useHistory();
  const [errors, setErrors] = useState<string[]>([]);

  async function crear(cliente: clienteCrearDTO) {
    try {
      // Envia los datos a la API para ejecutar el registro del nuevo cliente.
      const result = await axios.post(urlCliente, cliente);
      // Si retorna un valor vacio es por que ocurrio un error o algun campo requerido iba vacio.
      if (result.data !== "") {        
        history.push("/cliente");
        Swal.fire({
          title: "Se generó este nuevo id",
          text: result.data,
          icon: "success",
          position: "top",
          timer: 4000,
          showConfirmButton: false,
        });
      } else {
        Swal.fire({
          title: "Ocurrio un error.",
          text: "Revice que no haya un campo vacío.",
          icon: "error",
          position: "top",
          timer: 5000,
          showConfirmButton: false,
        });
      }
    } catch (error) {
      setErrors(error.response.data);
    }
  }

  return (
    <>
      <h3 className="text-center mt-4">Registro de cliente</h3>
      <hr />
      <p className="text-center">Registre su cliente en el sistema</p>
      <ShowErrors error={errors} />
      <Form_Cliente
        model={{ nombre: "", apellidos: "", telefono: "", activo: "" }}
        onSubmit={async (values) => {
          await crear(values);
        }}
      />
    </>
  );
}
