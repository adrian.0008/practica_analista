export interface clienteCrearDTO {
    nombre: string;
    apellidos: string;
    telefono: string;
    activo: string;
}

export interface clienteDTO{
    idCliente: number;
    nombre: string;
    apellidos: string;
    telefono: string;
    activo: string;
}
