import { urlCliente } from "../utils/endpoints";
import EntityIndex from "../utils/EntityIndex";
import { clienteDTO } from "./Modelo_Cliente";

export default function Lista_Cliente() {
  return (
    <>
      <EntityIndex<clienteDTO>
        url={urlCliente}
        urlCreate="/cliente/crear"
        title="Listado de clientes"
        title2="Consulte la informacion de sus clientes"
        entityName="cliente"
      >
        {(cliente) => (
          <>
            <thead className="table-dark">
              <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Apellidos</th>
                <th>Teléfono</th>
                <th>Activó</th>
              </tr>
            </thead>
            <tbody>
              {cliente?.map((cliente) => (
                <tr key={cliente.idCliente}>
                  <td>{cliente.idCliente}</td>
                  <td>{cliente.nombre}</td>
                  <td>{cliente.apellidos}</td>
                  <td>{cliente.telefono}</td>
                  {cliente.activo === "S" ? (
                    <td>Sí</td>
                  ) : (
                    <td>No</td>
                  )}
                </tr>
              ))}
            </tbody>
          </>
        )}
      </EntityIndex>
    </>
  );
}
