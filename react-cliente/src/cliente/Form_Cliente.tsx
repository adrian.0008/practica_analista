import { Formik, Form, FormikHelpers, ErrorMessage } from "formik";
import { Link } from "react-router-dom";
import * as Yup from "yup";
import FormGroupText from "../utils/FormGroupText";
import Button from "../utils/Button";
import { clienteCrearDTO } from "./Modelo_Cliente";
import ShowErrorField from "../utils/ShowErrorField";

export default function Form_Cliente(props: formClienteProps) {
  return (
    <Formik
      initialValues={props.model}
      onSubmit={props.onSubmit}
      validationSchema={Yup.object({
        nombre: Yup.string()
          .min(3, "La longitud mínima es de 3 caracteres.")
          .required("Este campo es requerido.")
          .max(20, "La longitud máxima es de 20 caracteres."),
        apellidos: Yup.string()
          .min(4, "La longitud mínima es de 4 caracteres.")
          .required("Este campo es requerido.")
          .max(50, "La longitud máxima es de 50 caracteres."),
        telefono: Yup.string()
          .min(8, "La longitud mínima es de 8 caracteres.")
          .max(12, "La longitud máxima es de 12 caracteres."),
        activo: Yup.string()
          .min(1, "Debe seleccionar un estado.")
          .required("Debe seleccionar un estado."),
      })}
    >
      {(formikProps) => (
        <Form>
          <div className="row mb-2">
            <div className="d-flex justify-content-center">
              <div className="col-12 col-md-10 col-lg-6">
                <div className="card p-sm-3 pb-5 p-2">
                  <FormGroupText
                    campo="nombre"
                    label="Nombre:"
                    required={true}
                  />
                  <FormGroupText
                    campo="apellidos"
                    label="Apellidos:"
                    required={true}
                  />
                  <FormGroupText campo="telefono" label="Teléfono:" />
                  <div className="mb-3">
                    <div className="input-group">
                      <span className="input-group-text" id="inputGroupPrepend">
                        Activó:<label className="text-danger">*</label>
                      </span>
                      <select
                        {...formikProps.getFieldProps("activo")}
                        className="form-select"
                        aria-describedby="inputGroupPrepend"
                      >
                        <option value="">-- Estado --</option>
                        <option value="S">Sí</option>
                        <option value="N">No</option>
                      </select>
                    </div>
                    <ErrorMessage name={"activo"}>
                      {(message) => <ShowErrorField message={message} />}
                    </ErrorMessage>
                  </div>
                  <div className="container mt-3">
                    <div className="row">
                      <div className="col-6 d-grid gap-2">
                        <Link
                          className="btn btn-outline-secondary"
                          to="/cliente"
                        >
                          Cancelar
                        </Link>
                      </div>
                      <div className="col-6 d-grid gap-2">
                        <Button
                          disabled={formikProps.isSubmitting}
                          type="submit"
                        >
                          Salvar
                        </Button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Form>
      )}
    </Formik>
  );
}

interface formClienteProps {
  model: clienteCrearDTO;
  onSubmit(
    values: clienteCrearDTO,
    accion: FormikHelpers<clienteCrearDTO>
  ): void;
}
