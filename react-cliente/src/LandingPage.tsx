import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faFaceGrin,
  faBuildingCircleArrowRight,
  faChargingStation,
  faBattery2,
} from "@fortawesome/free-solid-svg-icons";

export default function LandingPage() {

  return (
    <>
      <div className="container-fluid px-4 my-3">
        <div className="row g-4">
          <div className="col-12">
            <h3 className="text-center mt-5">Ejercicio práctico analista</h3>
            <h5 className="text-center mt-5">Responsable: Adrián Aguilar R.</h5>
            <h6 className="text-center mt-5">Octubre 2022</h6>
          </div>
        </div>
      </div>
    </>
  );
}
