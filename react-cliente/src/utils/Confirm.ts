import Swal from "sweetalert2";

export default function confirm(
  onConfirm: any,
  title: string = "¿Desea borrar el registro?",
  textoBotonConfirmacion: string = "Borrar"
) {
    Swal.fire({
        title: title,
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#d33',
        cancelButtonColor: '#839192',
        cancelButtonText: 'Cancelar',
        confirmButtonText: textoBotonConfirmacion,
    }).then(result => {
        if (result.isConfirmed){
            onConfirm();
        }
    })
}
