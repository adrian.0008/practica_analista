import { NavLink } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faHouseChimney,
  faUserGroup,
  faPlus
} from "@fortawesome/free-solid-svg-icons";

export default function Menu() {
  const activeClass = "active";
  
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light fixed-top">
      <div className="container-fluid">
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target=".navbar-collapse"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto">
            <li className="nav-item ms-0 ms-lg-2">
              <NavLink
                className="nav-link"
                activeClassName={activeClass}
                to="/"
              >
                <FontAwesomeIcon icon={faHouseChimney} /> Inicio
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                className="nav-link"
                activeClassName={activeClass}
                to="/cliente"
              >
                <FontAwesomeIcon icon={faUserGroup} /> Lista clientes
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                className="nav-link"
                activeClassName={activeClass}
                to="/cliente/crear"
              >
                <FontAwesomeIcon icon={faPlus} /> Nuevo cliente
              </NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}
