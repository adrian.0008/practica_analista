import axios, { AxiosResponse } from "axios";
import { ReactElement, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import GenericList from "./GenericList";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faPlus,
} from "@fortawesome/free-solid-svg-icons";

export default function EntityIndex<T>(props: entityIndexProps<T>) {
  const [entity, setEntity] = useState<T[]>();

  useEffect(() => {
    loadData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  async function loadData() {
      await axios.get(props.url).then((respuesta: AxiosResponse<T[]>) => {
        setEntity(respuesta.data);
      });
  }

  return (
    <>
      <div className="container mt-4 mb-4">
        <div className="d-flex justify-content-between">
          <h3></h3>
          <h3 className="text-center smallTitle">{props.title}</h3>
          <Link
            className="btn btn-outline-dark"
            to={props.urlCreate}
            title="Nuevo"
          >
            <FontAwesomeIcon icon={faPlus} />
          </Link>
        </div>
        <p className="text-center pe-0 pe-md-4">{props.title2}</p>
      </div>

      <GenericList list={entity}>
        <div className="table-responsive">
          <table className="table table-striped table-hover">
            {props.children(entity!)}
          </table>
        </div>
      </GenericList>
    </>
  );
}

interface entityIndexProps<T> {
  url: string;
  urlCreate: string;
  children(entity: T[]): ReactElement;
  title: string;
  title2: string;
  entityName: string;
}
